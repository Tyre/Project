![](Tyre.png)

The `Type` programming language infrastructure consists of multiple programming languages and subsystems.

# Why multiple languages?

Different languages are suitable for different tasks.
There could just be one language, which supports features of higher and lower levels, but such a language might need to do some compromises for one of them.

Instead making different languages and making using multiple of them together as easy as possible seems like a better solution.

Besides there might be people liking some of the features only, but not wanting to deal with the other features, for example because some are too insecure, or too high level for their tasks, but don't want to use a bloated language, might still want to use a rather minimalistic language, which also just works well by itself.

It might also be possible to add your own langauges with different focus using this infrastructure.

# The inner representation

All languages are parsed from simple s-expressions.
Every s-expression is a token, which is either a symbol or a list.
A symbol is basically just a string, a list contains tokens, which can either be symbols or lists once more.

This inner representation might be created by any kind of language:

* writing the program using s-expression syntax directly
* writing some simplified notation, which almost directly maps to simple s-expressions
  * indentation based syntax
  * special parentheses for special meanings
  * infix notations
  * configurable reader macros
* converting some lanugage, which is not based on s-expressions, into simple s-expressions
  * existing languages
    * might need some changes to map correctly to the language
    * optionally some additions to cover all features
  * languages inspired by existing languages
  * a new langauge
* using a graphical programming language, which maps to simple s-expressions
* map spoken text to simple s-expressions

# Parsers

Each language will come in two flavours:
* as library
* as binary

The library will not contain any parsers, so you can include the parsers yourself.

The binary will contain a few parsers, which can be selected by default.
Parsers might also be developed by users and might be added in pull requests.

# The language layers

The language is built in three layers, each of them a working programming language on its own:

* [T](https://gitlab.com/Tyre/T): Just a nice `C`
* `Ty`: All kinds of types
* `Tyr`: The real language

It's possible to share exposed definitions between two adjacent layers.

## T

![](T.png)

The bottom layer `T` aims to be close to `C`, but much more consistent.

Like in `C`, functions are statically typed. Generics do not exist, so every function name has fixed argument types.
There is a clear distinction between toplevel and bodies.
Only a few forms are possible in toplevel, and most other forms can only be in bodies.

There's no difference between functions and structs.
Functions are just callable structs.
Basically the parameter list of every function can be stored as a struct to be reused.
And basically every struct can be called as a function.

Unlike `C`, it's normally not necessary to specify most of the types.
In most cases, it's inferred. Only the primitive types and function argument types have to be specified explicitly, composed types and function return types are always inferred.
Parameters can be supplied by order or by name.

There are no namespaces. Included files are interpreted like just being appended to the file.
The program starts with the `main` function, just like `C`.

It's also possible to call `C` functions directly from `T` in some way.

## Ty

![](Ty.png)

The mid layer `Ty` aims to be similar to modern high level langauges like `C++` and `Rust`.
The most important additions are variant types and generics.
All forms are valid at any level, even function definitions. There's no need for a `main` function. The program just starts at the beginning.
So there exist nested functions and also namespaces.

Functions can be generic.
The dispatch happens at compile time for constant values.

It's porbably the most expressive language, but also the most complicated.
It's reasonable to define types with complicated special behaviour in `Ty` and then expose them to `Tyr`.

## Tyr

![](Tyr.png)

The top layer `Tyr` aims become fully independant of order without introducing keywords as a new concept, whereever reasonable, and uses types to achieve that.
A type just represents a set of values, and the same operations apply to types and values.
Functions are not named and don't exist as types or values. There's only one global multi method, which dispatches on argument types.
The dispatch just works like in `Ty`.
`Tyr` can be seen as a restricted and simpified version of `Ty`.

But this restriction is useful to get rid of argument order.
You'll see later, how this is not really a restriction.
